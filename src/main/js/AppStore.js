import { createStore } from 'redux'

const initialState = {
    user: {
        id: null,
        role: 'HOST'
    }

};

function appReducer(state = {}, action) {
    switch (action.type) {
        case 'ADD_TODO':
            return state.concat([action.text])
        case 'LOGIN':
            return state.user = action.user;
        case 'LOGOUT':
            return initialState;
        default:
            return state
    }
}

export default createStore(appReducer, initialState);