import React from "react";
import ReactDOM from 'react-dom'

class IndexPage extends React.Component {

    render(){
        return <div className="container">
            <h1>Welcome on my index page</h1>
        </div>;
    }

}

export default IndexPage;