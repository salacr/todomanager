import React from "react";
import ReactDOM from 'react-dom'

class TodoList extends React.Component{
    render() {
        var todos = this.props.todos.map(todo =>
            <TodoListItem key={todo.key} todo={todo}/>
        );
        return (
            <table>
                <tbody>
                <tr>
                    <th>Title</th>
                    <th>Content</th>
                    <th>Due Date</th>
                </tr>
                {todos}
                </tbody>
            </table>
        )
    }
}

class TodoListItem extends React.Component{
    render() {
        return (
            <tr>
                <td>{this.props.todo.title}</td>
                <td>{this.props.todo.content}</td>
                <td>{this.props.todo.dueDate}</td>
            </tr>
        )
    }
}

class TodoPage extends React.Component {

    render(){
        return <div className="container">
            <TodoList />
        </div>;
    }

}

export default TodoPage;