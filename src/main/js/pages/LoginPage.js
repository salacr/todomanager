import React from "react";
import ReactDOM from 'react-dom'
import LoginForm from './../LoginForm'

class LoginPage extends React.Component {

    render(){
        return <div className="container">
            <LoginForm />
        </div>;
    }

}

export default LoginPage;