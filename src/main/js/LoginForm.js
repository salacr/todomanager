import React from 'react';
import ReactDOM from 'react-dom';

import { Field, reduxForm } from 'redux-form'

import renderField from './FormCommon'

const LoginForm = (props) => {
    const { error, handleSubmit } = props
    return (
        <form onSubmit={handleSubmit}>
            <Field name="username" type="text" component={renderField} label="Username"/>
            <Field name="password" type="password" component={renderField} label="Password"/>
            {error && <strong>{error}</strong>}
            <div>
                No submit button in the form. The submit button below is a separate unlinked component.
            </div>
        </form>
    )
}

export default reduxForm({
    form: 'LoginForm'  // a unique identifier for this form
})(LoginForm)