import React from 'react'
import ReactDOM from 'react-dom'
import AppStore from './AppStore'
import MainMenu from './MainMenu'
import MainContent from './MainContent'
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'


class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {todos: []};
    }

    componentDidMount() {
        this.setState({todos: [{key: 1, title: 'Title 1', content: 'Content'}]});
    }

    render() {
        return (
            <div>
                <MainMenu />
                <MainContent />
            </div>
        )
    }
}


// Provider tag mi dela problem
ReactDOM.render(
    <Provider store={AppStore}>
        <BrowserRouter>
            <App />
        </BrowserRouter>
    </Provider>,
    document.getElementById('react')
)