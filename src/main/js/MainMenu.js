import React from 'react'
import ReactDOM from 'react-dom'
import { NavLink } from 'react-router-dom'


class MainMenu extends React.Component {

    logout(){

    }

    render() {
        return <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <a className="navbar-brand" href="/">Todo-app</a>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup"
                    aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
                    <NavLink activeClassName="active" className="nav-item nav-link" to="/">Home</NavLink>
                    <NavLink activeClassName="active" className="nav-item nav-link" to="/todo-list">Todo list</NavLink>
                    <NavLink activeClassName="active" className="nav-item nav-link" to="/login">Login</NavLink>
                    <NavLink activeClassName="active" className="nav-item nav-link" onClick={this.logout} >Logout</NavLink>
                </div>
            </div>
        </nav>
    }
}

export default MainMenu;