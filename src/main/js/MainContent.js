import React from 'react'
import ReactDOM from 'react-dom'
import { Switch, Route } from 'react-router'
import IndexPage from './pages/IndexPage'
import LoginPage from './pages/LoginPage'
import TodoPage from './pages/TodoPage'


class MainContent extends React.Component {

    render() {
        return <Switch>
            <Route exact path="/" component={IndexPage}/>
            <Route path="/login" component={LoginPage}/>
            <Route path="/todo-list" component={TodoPage}/>
        </Switch>
    }
}

export default MainContent;