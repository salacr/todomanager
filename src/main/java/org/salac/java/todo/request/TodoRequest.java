package org.salac.java.todo.request;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

public class TodoRequest {

    @NotNull
    private String title;

    @NotNull
    private String content;

    private LocalDateTime dueDate;

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }
}
