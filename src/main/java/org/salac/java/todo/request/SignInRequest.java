package org.salac.java.todo.request;

import org.salac.java.todo.dto.Role;
import org.salac.java.todo.dto.UserLoginDTO;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public class SignInRequest{

    @NotNull
    @Pattern(regexp = ".+@.+\\..+", message = "Value isn't valid email address")
    private String email;


    @NotNull
    private String password;

    public SignInRequest() {
    }

    public SignInRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserLoginDTO createUserLogin(PasswordEncoder passwordEncoder) {
        return new UserLoginDTO(email, passwordEncoder.encode(password), Role.ROLE_ADMIN);
    }
}