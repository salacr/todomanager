package org.salac.java.todo.service.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.salac.java.todo.dto.UserLoginDTO;
import org.salac.java.todo.dto.repository.UserLoginRepository;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

@Configurable
public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

    private final UserLoginRepository userLoginRepository;

    public JWTLoginFilter(String url, AuthenticationManager authManager, UserLoginRepository userLoginRepository) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authManager);
        this.userLoginRepository = userLoginRepository;
    }

    @Override
    public Authentication attemptAuthentication(
            HttpServletRequest req, HttpServletResponse res)
            throws AuthenticationException, IOException {
        AccountCredentials credentials = new ObjectMapper()
                .readValue(req.getInputStream(), AccountCredentials.class);
        return getAuthenticationManager().authenticate(
                new UsernamePasswordAuthenticationToken(
                        credentials.getEmail(),
                        credentials.getPassword(),
                        Collections.emptyList()
                )
        );
    }

    @Override
    protected void successfulAuthentication(
            HttpServletRequest req,
            HttpServletResponse res, FilterChain chain,
            Authentication auth) {

        UserLoginDTO userLoginDTO = userLoginRepository.findByEmail(auth.getName());

        TokenAuthenticationService
                .addAuthentication(res, auth.getName(), auth.getAuthorities().toArray()[0].toString(), userLoginDTO.getId());
    }
}
