package org.salac.java.todo.service.auth;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.UUID;

@Configurable
public class JWTAuthenticationToken extends UsernamePasswordAuthenticationToken {

    private UUID uuid;

    public JWTAuthenticationToken(Object principal, Object credentials) {
        super(principal, credentials);
    }

    public JWTAuthenticationToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }
}

