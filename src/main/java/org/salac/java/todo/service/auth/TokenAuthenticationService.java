package org.salac.java.todo.service.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.salac.java.todo.dto.Role;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Date;
import java.util.UUID;

public class TokenAuthenticationService {
    private static final long EXPIRATIONTIME = 864_000_000; // 10 days
    private static final String SECRET = "zo98413da";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";

    public static String createJwtToken(String username, UUID id, Role role) {

        Claims claims = Jwts.claims();
        claims.setSubject(username);
        claims.put("role", role.toString());
        claims.put("id", id.toString());

        return TOKEN_PREFIX + Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATIONTIME))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }

    static void addAuthentication(HttpServletResponse res, String username, String role, UUID id) {
        String JWT = createJwtToken(username, id, Role.valueOf(role));
        res.addHeader(HEADER_STRING, JWT);
    }

    public static Claims parseClaims(String token) {
        try {
            Claims claims = Jwts.parser()
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                    .getBody();
            return claims;
        }
        catch (Exception e)
        {
            throw new RuntimeException("Unable to process auth token:" + e.getMessage());
        }
    }

    static Authentication getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null && !token.isEmpty()) {
            // parse the token.
            Claims claims = parseClaims(token);

            String user = claims.getSubject();
            Role role = Role.valueOf(claims.get("role", String.class));
            UUID id = UUID.fromString(claims.get("id", String.class));

            if (user == null)
            {
                return null;
            }

            JWTAuthenticationToken jwtToken = new JWTAuthenticationToken(
                    user,
                    null,
                    Collections.singletonList(new SimpleGrantedAuthority(role.name()))
            );
            jwtToken.setUuid(id);
            return jwtToken;
        }
        return null;
    }
}

