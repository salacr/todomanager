package org.salac.java.todo.controller.rest;

import org.salac.java.todo.dto.TodoDto;
import org.salac.java.todo.dto.repository.TodoRepository;
import org.salac.java.todo.request.TodoRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/rest/todo")
public class TodoController {

    private TodoRepository repository;

    public TodoController(@Autowired TodoRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/list")
    public ResponseEntity<List<TodoDto>> list(@RequestParam(value="name", defaultValue="World") String name) {
        return ResponseEntity.ok(Collections.EMPTY_LIST);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TodoDto> get(@RequestParam(value="id") UUID id) {
        return ResponseEntity.ok(
                repository.findById(id).get()
        );
    }

    @PostMapping(value = "/create")
    public ResponseEntity<TodoDto> create(@Valid @RequestBody TodoRequest todoRequest, BindingResult bindingResult) {

        TodoDto todo = new TodoDto(todoRequest.getTitle(), todoRequest.getContent(), todoRequest.getDueDate());
        repository.save(todo);
        return ResponseEntity.ok(todo);
    }

    @PutMapping(value = "/update/{id}")
    public ResponseEntity<TodoDto> update(@RequestParam(value="id") UUID id, @Valid @RequestBody TodoRequest todoRequest, BindingResult bindingResult) {
        TodoDto todo = repository.findById(id).get();
        todo.setTitle(todoRequest.getTitle());
        todo.setContent(todoRequest.getContent());
        todo.setDueDate(todoRequest.getDueDate());
        return ResponseEntity.ok(todo);
    }

    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<UUID> delete(@RequestParam(value="id") UUID id) {
        repository.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
