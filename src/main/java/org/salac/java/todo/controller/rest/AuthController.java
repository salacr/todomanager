package org.salac.java.todo.controller.rest;


import org.salac.java.todo.dto.UserLoginDTO;
import org.salac.java.todo.dto.repository.UserLoginRepository;
import org.salac.java.todo.request.SignInRequest;
import org.salac.java.todo.service.auth.TokenAuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collections;

@RestController
@RequestMapping(path = "/rest/auth")
public class AuthController {

    private UserLoginRepository userRepository;

    private PasswordEncoder passwordEncoder;

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setUserRepository(UserLoginRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping(path = "/sign-in")
    public ResponseEntity<?> signIn(@Valid @RequestBody SignInRequest signIn, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new RuntimeException();
            //return new BadRequest(bindingResult);
        }
        UserLoginDTO userLoginDTO = userRepository.save(signIn.createUserLogin(passwordEncoder));
        HttpHeaders response = new HttpHeaders();
        response.put(TokenAuthenticationService.HEADER_STRING, Collections.singletonList(TokenAuthenticationService.createJwtToken(signIn.getEmail(), userLoginDTO.getId(), userLoginDTO.getRole())));
        return new ResponseEntity(response, HttpStatus.OK);
    }

}
