package org.salac.java.todo.dto.repository;

import org.salac.java.todo.dto.TodoDto;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface TodoRepository extends CrudRepository<TodoDto, UUID> {
}
