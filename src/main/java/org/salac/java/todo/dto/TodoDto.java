package org.salac.java.todo.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.UUID;

@Entity
public class TodoDto {

    @Id
    @Column
    private UUID id;

    @Column
    private String title;

    @Column
    private String content;

    @Column
    private LocalDateTime dueDate;

    @Column
    private LocalDateTime createdDate;

    public TodoDto(String title, String content, LocalDateTime dueDate) {
        this.id = UUID.randomUUID();
        this.dueDate = LocalDateTime.now();
        this.title = title;
        this.content = content;
        this.dueDate = dueDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setDueDate(LocalDateTime dueDate) {
        this.dueDate = dueDate;
    }

    @Override
    public String toString() {
        return "TodoDto{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", dueDate=" + dueDate +
                ", createdDate=" + createdDate +
                '}';
    }
}
