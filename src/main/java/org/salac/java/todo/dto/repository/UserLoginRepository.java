package org.salac.java.todo.dto.repository;

import org.salac.java.todo.dto.UserLoginDTO;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface UserLoginRepository extends CrudRepository<UserLoginDTO, UUID> {

    UserLoginDTO findByEmail(String email);

}
