package org.salac.java.todo.dto;

import javax.persistence.*;
import java.util.UUID;

@javax.persistence.Entity
public class UserLoginDTO {

    @Id
    @Column
    private UUID id;

    @Column(unique = true)
    private String email;

    @Column
    private String password;

    @Column
    @Enumerated(EnumType.STRING)
    private Role role;

    public UserLoginDTO(String email, String password, Role role) {
        this.id = UUID.randomUUID();
        this.email = email;
        this.password = password;
        this.role = role;
    }

    public UUID getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Role getRole() {
        return role;
    }
}

